const cliProgress = require("cli-progress");
const _colors = require("colors");
const fs = require("fs");
const faker = require("faker");
const moment = require("moment");
const mustache = require("mustache");
const fetch = require("node-fetch");
const uuid = require("uuid");
const { Client } = require("pg");
const client = new Client({
  host: "localhost",
  database: "kronologic",
  port: 22222,
  user: "kronologic_dev",
  password: "kronologic_dev",
});

const ACCEPTED = "accepted";
const CANCELLED = "cancelled";
const DECLINED = "declined";
const ENGAGED_NO_INTENT = "engaged_no_intent";
const INIT_FAULT = "init_fault";
const INITIALIZED = "initialized";
const INITIALIZING = "initializing";
const NEGOTIATING_IN_PROGRESS = "negotiation_in_progress";
const NO_RESPONSE = "no_response";
const REQUIRES_USER_INTERVENTION = "requires_user_intervention";
const USER_INTERVENED = "user_intervened";
const WAITING_FOR_FIRST_RESPONSE = "waiting_for_first_response";

const SCHEDULED_MEETINGS = [
  USER_INTERVENED,
  WAITING_FOR_FIRST_RESPONSE,
  NEGOTIATING_IN_PROGRESS,
  ACCEPTED,
];

function toState(s) {
  if (s !== ACCEPTED || s !== DECLINED) {
    return "response_needed";
  }
  if (s === CANCELLED) {
    return "cancelled_error";
  }
  return s;
}

const STATUSES = [
  ACCEPTED,
  CANCELLED,
  DECLINED,
  ENGAGED_NO_INTENT,
  INIT_FAULT,
  INITIALIZED,
  INITIALIZING,
  NEGOTIATING_IN_PROGRESS,
  NO_RESPONSE,
  REQUIRES_USER_INTERVENTION,
  USER_INTERVENED,
  WAITING_FOR_FIRST_RESPONSE,
];

const BEARER_TOKEN = process.env.JWT_TOKEN;

function readFile(file) {
  return new Promise((resolve, reject) => {
    fs.readFile(`${__dirname}/${file}.mustache`, (err, data) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(data.toString());
    });
  });
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function createContact(contact) {
  return fetch("http://localhost:8001/api/contact/", {
    method: "POST",
    body: JSON.stringify(contact),
    headers: {
      "jwt-token": BEARER_TOKEN,
    },
  });
}

function createDefinition(definition) {
  return fetch("http://localhost:8001/api/meetings/definition", {
    method: "POST",
    body: JSON.stringify(definition),
    headers: {
      "jwt-token": BEARER_TOKEN,
    },
  });
}

function createMeetings(meetings) {
  return fetch("http://localhost:8001/api/meetings/instance", {
    method: "POST",
    body: JSON.stringify(meetings),
    headers: {
      "jwt-token": BEARER_TOKEN,
    },
  });
}

function getBar(message) {
  return new cliProgress.SingleBar({
    format:
      `${message} |` +
      _colors.cyan("{bar}") +
      "| {percentage}% || {value}/{total} Records",
    barCompleteChar: "\u2588",
    barIncompleteChar: "\u2591",
    hideCursor: true,
  });
}

async function main() {
  await client.connect();
  try {
    const userIdToCalendar = {};
    const cleanupTemplate = await readFile("cleanup");
    const teamsTemplate = await readFile("teams");
    const insertCalendarsTemplate = await readFile("insert_calendar");
    const insertUsersTemplate = await readFile("insert_users");
    const insertIntegrationsTemplate = await readFile("insert_integrations");
    const insertCalendarEventTemplate = await readFile("insert_calendar_event");
    const insertMeetingsTemplate = await readFile("insert_meetings");
    const users = [];
    const calendars = [];
    const calender_events = [];
    const contacts = [];
    const definitions = [];
    let meetings = [];
    const teams = [];
    client.query(cleanupTemplate);
    let bar = getBar("Creating Contacts");
    const numberOfContacts = 100;
    bar.start(numberOfContacts, 0);
    for (let i = 0; i < numberOfContacts; i++) {
      bar.increment();
      const card = faker.helpers.createCard();
      const { email, name } = card;
      const nameParts = name.split(" ");
      const lastName = nameParts[nameParts.length - 1];
      const contact = {
        email,
        firstName: nameParts.slice(0, nameParts.length - 1).join(" "),
        lastName,
      };
      try {
        const response = await createContact(contact);
        const { id } = await response.json();
        contact.id = id;
        contacts.push(contact);
      } catch (error) {
        console.log(error);
      }
    }
    bar.stop();
    bar = getBar("Creating Definitions");
    for (let i = 0; i < 100; i++) {
      bar.increment();
      const definition = {
        enabled: true,
        team: 1,
        routing: "random",
        distribution: "random",
        inviteTemplates: [
          {
            id: 0,
            location: "{{contact_logic_field}}",
            notes: "invite template content...",
            title: "{{contact_first_name}}",
          },
        ],
        emailTemplates: [
          {
            body: "email template content...<br/>",
            order: 1,
            title: "{{meeting_duration}}",
          },
        ],
        name: `${faker.commerce.productAdjective()} ${faker.commerce.product()} Sales`,
        description: getRandomInt(1000000).toString(),
        recycle_after_hours: 0,
        buffer_duration_mins: 180,
        properties: {
          bcc: "",
          gap: 0,
          dayRange: {
            to: 30,
            from: 1,
          },
          duration: 30,
          routeOnActivation: false,
        },
        routing_logic: {},
      };
      try {
        const response = await createDefinition(definition);
        const { id } = await response.json();
        definition.id = id;
        definitions.push(definition);
      } catch (error) {
        // Skip
      }
    }
    bar.stop();
    bar = getBar("Creating Users");
    bar.start(100, 0);
    for (let i = 0; i < 100; i++) {
      bar.increment();
      const card = faker.helpers.createCard();
      const { company, email, name } = card;
      const nameParts = name.split(" ");
      const lastName = nameParts[nameParts.length - 1];
      const user = {
        email,
        firstName: nameParts.slice(0, nameParts.length - 1).join(" "),
        lastName,
        title: company.name,
        role: Math.floor(Math.random() * (3 - 1)) + 1,
      };
      const userQuery = mustache.render(insertUsersTemplate, { users: [user] });
      const result = await client.query(userQuery);
      const [{ id }] = result.rows;
      user.id = id;
      users.push(user);
      const calendar = {
        internalId: uuid.v4(),
        email,
        userId: user.id,
      };
      const calendarQuery = mustache.render(insertCalendarsTemplate, {
        calendars: [calendar],
      });
      {
        const { rows } = await client.query(calendarQuery);
        const [newCalendar] = rows;
        calendars.push(newCalendar);
        userIdToCalendar[user.id] = newCalendar;
      }
      const team = {
        user_id: id,
        team_id: 1,
      };
      const teamQuery = mustache.render(teamsTemplate, { teams: [team] });
      await client.query(teamQuery);
      teams.push(team);
      const integrationQuery = mustache.render(insertIntegrationsTemplate, {
        integrations: [
          {
            userId: user.id,
          },
        ],
      });
      await client.query(integrationQuery);
    }
    bar.stop();
    const definitionIds = definitions.map((d) => d.id);
    const contactIds = contacts.map((c) => c.id);
    const userIds = users.map((u) => u.id);
    bar = getBar("Creating Meetings");
    bar.start(numberOfContacts * definitionIds.length, 0);
    for (let i = 0; i < contactIds.length; i++) {
      for (let j = 0; j < definitionIds.length; j++) {
        bar.increment();
        const attempts_total = getRandomInt(3);
        const attempts = getRandomInt(attempts_total);
        const newMeeting = {
          meeting_definition_id: definitionIds[j],
          user_id: userIds[getRandomInt(99)],
          active: true,
          contact_id: contactIds[i],
          attempts,
          attempts_total,
          status: STATUSES[getRandomInt(STATUSES.length)],
        };
        const meetingQuery = mustache.render(insertMeetingsTemplate, {
          meetings: [newMeeting],
        });
        const result = await client.query(meetingQuery);
        const { rows } = result;
        const [meeting] = rows;
        const { id } = meeting;
        meeting.id = id;
        meetings.push(meeting);
      }
    }
    bar.stop();
    bar = getBar("Creating Calendar Events");
    const scheduledMeetings = meetings.filter((m) =>
      SCHEDULED_MEETINGS.includes(m.status)
    );
    bar.start(scheduledMeetings.length, 0);
    for (let i = 0; i < scheduledMeetings.length; i++) {
      bar.increment();
      const meeting = scheduledMeetings[i];
      let day = getRandomInt(400);
      if (day === 15) {
        day = day-1;
      }
      const event = {
        id: uuid.v4(),
        name: "A COVID-19 Gathering",
        start: moment()
          .subtract(day, "days")
          .format(),
        calendarId: userIdToCalendar[meeting.user_id].id,
        contactId: meeting.contact_id,
        meetingId: meeting.id,
        end: moment().endOf("year").format(),
        userId: meeting.user_id,
        title: "title",
        location: "location",
        notes: "notes",
        description: "description",
        timezone: "America/Chicago",
        sequence: "",
        state: toState(meeting.status),
        ical_uid: "",
      };
      calendarEvent = mustache.render(insertCalendarEventTemplate, {
        events: [event],
      });
      await client.query(calendarEvent);
    }
    bar.stop();
  } finally {
    await client.end();
  }
  console.info("done...");
}

main().catch((error) => {
  console.error(error);
});
